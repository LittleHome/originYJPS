<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><decorator:title default="欢迎使用文档管理系统"/></title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
    <decorator:head/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script>
        $(function(){
            var winWidth,winHeight;
            Resize = function(){
                winWidth = $(window).width(),
                        winHeight= $(window).height();
            }
        });
    </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
<div class="bs-docs-example">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
<s:if test="#session.loginUser!=null">
    欢迎[${loginUser.username }]登录我们的系统&nbsp;
    <shiro:hasRole name="newsAdmin">
        <a href="<s:url var="news_newsList_url" action="news_newsList"/><c:url value="${news_newsList_url}"/>">
            新闻管理
        </a>
    </shiro:hasRole>
        <a href="<s:url var="user_showSelf_url" action="user_showSelf"/><c:url value="${user_showSelf_url}"/>">
            个人信息
        </a>&nbsp;
        <a href="<s:url var="login_url" action="login" method="logout"/><c:url value="${login_url}"/>">
            退出系统
        </a>
</s:if>
           </div>
        </div>
    </div>
</div>
<s:else>
    <a href="<s:url action="login" method="loginInput"/>">用户登录</a>
</s:else>
<hr/>
<h3 align="center"><decorator:title default="文档管理系统"/></h3>
<decorator:body/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:10px;">
    CopyRight@2015<br/>
</div>
</body>
</html>