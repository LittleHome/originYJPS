<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
  <nav class="navbar navbar-fixed-top navbar-leaf">
  <div class="container">
  <div class="navbar-header">
  <a class="navbar-brand" href="#">JESSY</a>
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
  data-target="#navbarForTeacher">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse">
  <ul class="nav navbar-nav">
  <li><a href="<s:url var="login_home_url" action="login!home"/><c:url value="${login_home_url}"/>"><span class="glyphicon glyphicon-home"></span> 首页</a></li>
  <li><a href="<s:url var="user_showSelf_url" action="user_showSelf"/><c:url value="${user_showSelf_url}"/>"><span class="glyphicon glyphicon-user"></span>个人档案</a></li>
  <shiro:hasRole name="newsAdmin">
    <li>
      <a href="<s:url var="news_newsList_url" action="news_newsList"/><c:url value="${news_newsList_url}"/>">
        <span class="glyphicon glyphicon-news"></span>新闻管理
      </a>
    </li>
  </shiro:hasRole>
  <shiro:hasRole name="userAdmin">
    <li>
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="glyphicon glyphicon-tasks"></span>
        系统管理
        <span class="arrow"></span>
      </a>
      <ul style="color:white;" class="dropdown-menu"  role="menu">
        <li><a href="<s:url var="admin_userList_url" action="admin_userList"/><c:url value="${admin_userList_url}"/>">用户管理</a></li>
        <li class="divider"></li>
        <li><a href="<s:url var="admin_roleList_url" action="admin_roleList"/><c:url value="${admin_roleList_url}"/>">角色管理</a></li>
        <li class="divider"></li>
        <li><a href="<s:url var="admin_permissionList_url" action="admin_permissionList"/><c:url value="${admin_permissionList_url}"/>">权限管理</a></li>
      </ul>
    </li>
  </shiro:hasRole>
  </ul>


  <ul class="nav navbar-nav navbar-right">
  <li><a class="cursor-default" href="#">【${loginUser.username }】</a></li>
  <li>
  <a class=" navbar-link" href="<s:url var="login_url" action="login" method="logout"/><c:url value="${login_url}"/>">
  <span class="glyphicon glyphicon-log-out"></span> 注销
  </a>
  </li>
  </ul>
  </div>
  <!-- .navbar-collapse -->
  </div>
  <!-- .container -->
  </nav>