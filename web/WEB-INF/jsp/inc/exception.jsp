<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>欢迎使用文档管理系统</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/global.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script>
        $(function(){
            var winWidth,winHeight;
            Resize = function(){
                winWidth = $(window).width(),
                        winHeight= $(window).height();
            }
        });
    </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
    <nav class="navbar navbar-fixed-top navbar-leaf">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">JESSY</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbarForTeacher">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<s:url var="login_home_url" action="login!home"/><c:url value="${login_home_url}"/>"><span class="glyphicon glyphicon-home"></span> 首页</a></li>
                    <li><a href="<s:url var="user_showSelf_url" action="user_showSelf"/><c:url value="${user_showSelf_url}"/>"><span class="glyphicon glyphicon-user"></span>个人档案</a></li>
                    <shiro:hasRole name="newsAdmin">
                        <li>
                            <a href="<s:url var="news_newsList_url" action="news_newsList"/><c:url value="${news_newsList_url}"/>">
                                <span class="glyphicon glyphicon-news"></span>新闻管理
                            </a>
                        </li>
                    </shiro:hasRole>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="cursor-default" href="#">【${loginUser.username }】</a></li>
                    <li>
                        <a class=" navbar-link" href="<s:url var="login_url" action="login" method="logout"/><c:url value="${login_url}"/>">
                            <span class="glyphicon glyphicon-log-out"></span> 注销
                        </a>
                    </li>
                </ul>
            </div>
            <!-- .navbar-collapse -->
        </div>
        <!-- .container -->
    </nav>
    <div class="container" style="background-color:#DCEAF4; width:700px; height:500px;border-radius: 3px 3px 3px 3px;">
        <div class="main-content">
            <label>${exception.message}</label>
        </div>
    </div>
<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:700px;">
    CopyRight@2015<br/>
</div>
</body>
</html>
