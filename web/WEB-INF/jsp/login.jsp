<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>用户登录</title>
    <!-- Le styles -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/example-fixed-layout.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-2.0.3.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-carousel.js"/>"></script>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <style>
        .autoCollapse {
            position: relative;
            padding: 15px;
            width: 25%;
            height: 380px;
            min-height: 380px;
            font-size: 4em;
            transition: all .5s;
        }

        .autoCollapse.expanded {
            width: 50%;
            font-size: 9em;
            transition: all .5s;
        }
    </style>
</head>
<body>
<button style="float:right;" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#loginModel">
    <span class="glyphicon"></span> 登录
</button>
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="span4">
            <div id="myCarousel" class="carousel slide">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item"><img src="<c:url value="/resources/img/qq.jpg"/>" alt="HTML5 logo" width="1500" height="1500" /></div>
                    <div class="item"><img src="<c:url value="/resources/img/22519.jpg"/>" alt="JS logo" width="1500" height="1500" /></div>
                    <div class="item"><img src="<c:url value="/resources/img/21855.jpg"/>" alt="Schema.org logo" width="1500" height="1500" /></div>
                    <div class="item"><img src="<c:url value="/resources/img/qq.jpg"/>" alt="JSON logo" width="1500" height="1500" /></div>
                </div>
                <!-- Carousel nav -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<%--<div class="modal fade" id="loginModel" tabindex="-1" role="dialog">--%>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <b>登录</b>
                </h4>
            </div>
            <div class="modal-body">
                <s:form method="POST" action="login!login">
                    <form action="#">
                        <div class="form-group">
                            <input class="form-control" type="text" name="username" placeholder="用户名" required>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" name="password" placeholder="密码" required>
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="submit" class="btn btn-primary">登录</button>
                    </form>
                </s:form>
            </div>
        </div>
    </div>
<%--</div>--%>
    <script>

        $(document).ready(function () {
            //resizing
            var height = $(window).height() - 250;
            $(".autoCollapse").css("height", height);

            $(window).resize(function () {
                $(".autoCollapse").css("height", $(window).height() - 250);
            });

            //mouse listener
            $(".autoCollapse").mouseenter(function () {
                if (false == $(this).is(".expanded")) {
                    $(".autoCollapse.expanded").removeClass("expanded");
                    $(this).addClass("expanded");
                }
            });
        });
    </script>
<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:700px;">
    CopyRight@2015<br/>
</div>
</body>
</html>