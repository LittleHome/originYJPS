<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>欢迎使用文档管理系统</title>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/global.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <script>
    $(function(){
      var winWidth,winHeight;
      Resize = function(){
        winWidth = $(window).width(),
                winHeight= $(window).height();
      }
    });
  </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
  <jsp:include page="../../jsp/inc/head.jsp"/>

  <div class="container" style="background-color:#DCEAF4; width:1300px; height:500px;border-radius: 0px 0px 0px 0px;">
    <div class="btn-group pull-right" style="margin-bottom:8px;">
      <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addPermission">
        <span class="glyphicon"></span> 添加权限
      </button>
    </div>
    <p></p>
    <div class="main-content">

      <table class="table">
        <thead>
        <tr>
          <th>权限</th>
          <th>描述</th>
          <th>操作</th>
        </tr>
        </thead>
        <c>
        <c:forEach items="${requestScope.permissionList}" var="permission">
        <%--<s:iterator value="#request.permissionList" var="permission">--%>
          <tr class="success">
            <td>${permission.permissionName}</td>
            <td>${permission.per_description}</td>
            <td>
              <button onclick="editPermission('<c:url value="admin_loadPermission.action?permission.id=${permission.id}"/>')"
                      class="btn btn-primary btn-xs"
                      data-toggle="modal" data-target="#editPermission">编辑
              </button>
              &nbsp;&nbsp;
              <button class="btn btn-primary btn-xs"
                 href="javascript:void(0)" data-toggle="tooltip" title="删除"
                 onclick="if (confirm('确认要删除本记录吗？此操作将不可恢复！'))
                         location = '<c:url value="admin_deletePermission.action?permission.id=${permission.id}"/>';
                         return false;">删除</button>


            </td>
          </tr>
        <%--</s:iterator>--%>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
  <%--新添权限模态框--%>
  <div class="modal fade" id="addPermission" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">新添权限</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="admin_addPermission">
            <form action="admin_addPermission" method="POST">
              <div class="form-group">
                <label class="control-label" for="title">权限名</label>
                <input type="text" class="form-control" id="title" name="permission.permissionName" required>
              </div>
              <div class="form-group">
                <label for="content">描述</label>
                <textarea class="form-control resize-none" id="content" rows="7" name="permission.per_description" required></textarea>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                新添
              </button>
            </form>
          </s:form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function editPermission(path){
      $.ajax({
        url: path,
        type: 'get',
        async: true,
        dataType:'json',
        success: function (permission) {
          $('#edit_permission').val(permission.permissionName);
          $('#edit_description').val(permission.per_description);
        }
      });
    }

  </script>
  <%--编辑修改权限模态框--%>
  <div class="modal fade" id="editPermission" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">编辑</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="admin_updatePermission">
            <form action="#">
              <div class="form-group">
                <label class="control-label" for="title">权限名</label>
                <input type="text" class="form-control" id="edit_permission" name="permission.permissionName" required>
              </div>
              <div class="form-group">
                <label for="content">内容</label>
                <textarea class="form-control resize-none" id="edit_description" rows="7" name="permission.per_description" required></textarea>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                确定
              </button>
            </form>
          </s:form>
        </div>
      </div>
    </div>
  </div>
<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:500px;">
  CopyRight@2015<br/>
</div>
</body>
</html>
