<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>欢迎使用文档管理系统</title>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/global.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <script>
    $(function(){
      var winWidth,winHeight;
      Resize = function(){
        winWidth = $(window).width(),
                winHeight= $(window).height();
      }
    });
  </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
  <jsp:include page="../../jsp/inc/head.jsp"/>
  <script type="text/javascript">
    function loadRole(path){
      $.ajax({
        url: path,
        type: 'get',
        async: true,
        dataType:'json',
        success: function (roles) {
          $.each(roles, function(idx, obj) {
            var inp = '<input type="checkbox" name="roles" value="' + obj.id + '"/>' + '&nbsp;&nbsp;' + obj.rolename;
            $("#per").append(inp);
          });
        }
      });
    }
  </script>
  <div class="container" style="background-color:#DCEAF4; width:1300px; height:500px;border-radius: 0px 0px 0px 0px;">
    <div class="btn-group pull-right" style="margin-bottom:8px;">
      <button onclick="loadRole('<c:url value="admin_findAllRoles.action"/>')" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addUser">
        <span class="glyphicon"></span> 添加用户
      </button>
    </div>
    <p></p>
    <div class="main-content">

      <table class="table">
        <thead>
        <tr>
          <th>用户昵称</th>
          <th>手机</th>
          <th>邮箱</th>
          <th>真实名称</th>
          <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.userPager.datas}" var="user">
          <%--<s:iterator value="#request.roleList" var="role">--%>
          <tr class="success">
            <td>${user.username}</td>
            <td>${user.mobile}</td>
            <td>
                ${user.email}
            </td>
            <td>
                ${user.realName}
            </td>
            <td>
              <button onclick="editUser('<c:url value="admin_loadUser.action?user.id=${user.id}"/>')"
                      class="btn btn-primary btn-xs"
                      data-toggle="modal" data-target="#editUser">编辑
              </button>
              &nbsp;&nbsp;
              <button class="btn btn-primary btn-xs"
                      href="javascript:void(0)" data-toggle="tooltip" title="删除"
                      onclick="if (confirm('确认要删除本记录吗？此操作将不可恢复！'))
                              location = '<c:url value="admin_deleteUser.action?user.id=${user.id}"/>';
                              return false;">删除</button>


            </td>
          </tr>
          <%--</s:iterator>--%>
        </c:forEach>
        <tr>
          <td colspan="7"><jsp:include page="../inc/pager.jsp">
            <jsp:param value="admin_userList.action" name="url" />
            <jsp:param value="${userPager.totalRecord }" name="items" />
            <jsp:param value="isRead" name="params" />
          </jsp:include></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>

  <%--发布新闻模态框--%>
  <div class="modal fade" id="addUser" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">编辑</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="admin_addUser">
              <div class="form-group">
                <label class="control-label" for="title">用户名</label>
                <input type="text" class="form-control" id="title" name="user.username" required>
              </div>
              <div class="form-group">
                <label for="content">手机</label>
                <input class="form-control" id="content"  name="user.mobile" required/>
              </div>
            <div class="form-group">
              <label class="control-label" for="email">邮箱</label>
              <input type="text" class="form-control" id="email" name="user.email" required>
            </div>
            <div class="form-group">
              <label for="realName">真实姓名</label>
              <input class="form-control" id="realName" name="user.realName" required/>
            </div>
              <div class="form-group" id="per">
                <label for="r">角色 </label>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                添加
              </button>
          </s:form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">

    function editUser(path){
      $.ajax({
        url: path,
        type: 'get',
        async: true,
        dataType:'json',
        success: function (user) {
          /*var temp = '<input class="edit_check" type="checkbox" name="permissions" />';
           $("#edit_per").removeChild(temp);*/

          $('#edit_username').val(user.username);
          $('#edit_mobile').val(user.mobile);
          $('#edit_email').val(user.email);
          $('#edit_realName').val(user.realName);

          /*$.each(role.allPermissions, function(idx, obj) {
            var inp = '<input class="edit_check" type="checkbox" name="permissions" value="' + obj.id + '"/>' + '&nbsp;&nbsp;' + obj.per_description;
            $("#edit_per").append(inp);
            if(role.viewPermissions == obj.per_description){
            }
          });*/
        }
      });
    }

  </script>
  <%--编辑修改新闻模态框--%>
  <div class="modal fade" id="editUser" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">编辑</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="admin_updateUser">
            <form action="#">
              <div class="form-group">
                <label class="control-label" for="edit_username">用户名</label>
                <input type="text" class="form-control" id="edit_username" name="user.username" required>
              </div>
              <div class="form-group">
                <label for="edit_mobile">手机</label>
                <input class="form-control" id="edit_mobile"  name="user.mobile" required/>
              </div>
              <div class="form-group">
                <label class="control-label" for="edit_email">邮箱</label>
                <input type="text" class="form-control" id="edit_email" name="user.email" required>
              </div>
              <div class="form-group">
                <label for="edit_realName">真实姓名</label>
                <input class="form-control" id="edit_realName" name="user.realName" required/>
              </div>
              <div id="edit_per" class="form-group">
                <label for="edit_r">角色</label>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                确定
              </button>
            </form>
          </s:form>
        </div>
      </div>
    </div>
  </div>
<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:500px;">
  CopyRight@2015<br/>
</div>
</body>
</html>
