<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>欢迎使用文档管理系统</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/global.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script>
        $(function(){
            var winWidth,winHeight;
            Resize = function(){
                winWidth = $(window).width(),
                        winHeight= $(window).height();
            }
        });
    </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
<%--<s:if test="#session.loginUser!=null">--%>
    <jsp:include page="../../jsp/inc/head.jsp"/>

    <div class="container" style="background-color:#DCEAF4; width:1300px; height:500px;border-radius: 0px 0px 0px 0px;">
        <div class="main-content">
            <section class="main-panel">
                <s:form method="POST" action="user_edit">
                    <div class="row">
                        <div class="col-xs-3 item-name">昵称：</div>
                        <div class="col-xs-6 item-content">${loginUser.username}</div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3 item-name edit">手机号码：</div>
                        <div class="col-xs-6 item-content">
                            <input class="form-control" type="text" name="mobile" required="" value="${loginUser.mobile}"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3 item-name">电子邮件：</div>
                        <div class="col-xs-6 item-content">
                            <input class="form-control" type="text" name="email" value="${loginUser.email}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 item-name">真实姓名：</div>
                        <div class="col-xs-6 item-content">
                            <input class="form-control" type="text" name="realName" value="${loginUser.realName}"/>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-success btn-lg" type="submit">保 存</button>
                        </div>
                    </div>
                </form>
                </s:form>
            </section>
        </div>
    </div>

<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:700px;">
    CopyRight@2015<br/>
</div>
</body>
</html>
