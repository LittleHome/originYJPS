<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>欢迎使用文档管理系统</title>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main1.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/global.css"/>">
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
  <script src="<c:url value="/resources/js/jquery.js"/>" type="text/javascript"></script>
  <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
  <script>
    $(function(){
      var winWidth,winHeight;
      Resize = function(){
        winWidth = $(window).width(),
                winHeight= $(window).height();
      }
    });
  </script>
</head>
<body onResize="Resize()" onLoad="Resize()">
<%--<s:if test="#session.loginUser!=null">--%>

  <jsp:include page="../../jsp/inc/head.jsp"/>
  <div class="container" style="background-color:#DCEAF4; width:1300px; height:500px;border-radius: 0px 0px 0px 0px;">
    <div class="btn-group pull-right" style="margin-bottom:8px;">
      <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addNews">
        <span class="glyphicon glyphicon-envelope"></span> 发布新闻
      </button>
    </div>
    <p></p>
    <div class="main-content">

      <table class="table">
        <thead>
        <tr>
          <th>标题</th>
          <th>内容</th>
          <th>发布时间</th>
          <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="#request.newsPager.datas" var="news">
          <tr class="success">
            <td>${news.title}</td>
            <td>
              <a onclick="checkNews('<c:url value="news_loadNews.action?id=${news.id}"/>')" data-toggle="modal" data-target="#checkNews">
                <c:if test="${news.content.length() gt 10}">新闻内容......</c:if>
                <c:if test="${news.content.length() le 10}">${news.content}</c:if>
              </a>
            </td>
            <td>${news.create_time}</td>
            <td>
              <button onclick="editNews('<c:url value="news_loadNews.action?id=${news.id}"/>')"
                      class="btn btn-primary btn-xs"
                      data-toggle="modal" data-target="#editNews">编辑
              </button>
              &nbsp;&nbsp;
              <a class="btn btn-primary btn-xs"
                 href="javascript:void(0)" data-toggle="tooltip" title="删除"
                 onclick="if (confirm('确认要删除本记录吗？此操作将不可恢复！'))
                         location = '<c:url value="news_deleteNews.action?id=${news.id}"/>';
                         return false;"><span class="glyphicon glyphicon-trash"></span></a>


            </td>
          </tr>
        </s:iterator>
        <tr>
          <td colspan="7"><jsp:include page="../inc/pager.jsp">
            <jsp:param value="news_newsList.action" name="url" />
            <jsp:param value="${newsPager.totalRecord }" name="items" />
            <jsp:param value="isRead" name="params" />
          </jsp:include></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
  <%--发布新闻模态框--%>
  <div class="modal fade" id="addNews" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">编辑新闻</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="news_addNews">
            <form action="news_addNews" method="POST">
              <div class="form-group">
                <label class="control-label" for="title">标题</label>
                <input type="text" class="form-control" id="title" name="title" required>
              </div>
              <div class="form-group">
                <label for="content">内容</label>
                <textarea class="form-control resize-none" id="content" rows="7" name="content" required></textarea>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                发布
              </button>
            </form>
          </s:form>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function editNews(path){
      $.ajax({
        url: path,
        type: 'get',
        async: true,
        dataType:'json',
        success: function (news) {
          $('#edit_title').val(news.title);
          $('#edit_content').val(news.content);
        }
      });
    }
    function checkNews(path){
      $.ajax({
        url: path,
        type: 'get',
        async: true,
        dataType:'json',
        success: function (news) {
          $('#check_title').html(news.title);
          $('#check_content').html(news.content);
        }
      });
    }
  </script>
  <%--编辑修改新闻模态框--%>
  <div class="modal fade" id="editNews" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">编辑新闻</h3>
        </div>
        <div class="modal-body">
          <s:form method="POST" action="news_updateNews">
            <form action="#">
              <div class="form-group">
                <label class="control-label" for="title">标题</label>
                <input type="text" class="form-control" id="edit_title" name="title" required>
              </div>
              <div class="form-group">
                <label for="content">内容</label>
                <textarea class="form-control resize-none" id="edit_content" rows="7" name="content" required></textarea>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">
                发布
              </button>
            </form>
          </s:form>
        </div>
      </div>
    </div>
  </div>
  <%--查看新闻--%>
  <div class="modal fade" id="checkNews" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <h3 class="modal-title">新闻详情</h3>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label" for="title">标题</label>
            <span class="form-control" id="check_title" name="title"></span>
          </div>
          <div class="form-group">
            <label for="content">内容</label>
            <span class="form-control resize-none" id="check_content" rows="7" name="content" ></span>
          </div>
        </div>
      </div>
    </div>
  </div>
<hr/>
<div align="center" style="width:100%;border-top:1px solid; float:left;margin-top:500px;">
  CopyRight@2015<br/>
</div>
</body>
</html>
