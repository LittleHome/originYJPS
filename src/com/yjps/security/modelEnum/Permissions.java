package com.yjps.security.modelEnum;

/**
 * Created by pcc on 2015/7/14.
 */
public enum Permissions {
    SUPER_ADMIN("user:*","系统的超级管理员,拥有系统业务增删改查权限"),
    NEWS_ADMIN("news:*","新闻管理，拥有新闻的增删改查权限");

    private String abbreviation;
    private String description;

    Permissions(String abbreviation, String description) {
        this.abbreviation = abbreviation;
        this.description = description;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getDescription() {
        return description;
    }
}
