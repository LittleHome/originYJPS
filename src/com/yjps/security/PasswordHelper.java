package com.yjps.security;

import com.yjps.model.User;
import org.apache.shiro.crypto.hash.Sha512Hash;

/**
 * Created by jessy on 2015/7/13.
 */
public class PasswordHelper {
    public static final String ps = "ps";

    public static User generatePassword(User user){
        String passwordHash = new Sha512Hash(user.getPassword(), user.getUsername() + ps, 99).toString();
        user.setPassword(passwordHash);
        return user;
    }

    public static void main(String[] args) {
		String passwordHash = new Sha512Hash("123", "xixi" + ps, 99).toString();
		System.out.println(passwordHash);

//		System.out.println("Default Charset=" + Charset.defaultCharset());
		System.out.println("file.encoding=" + System.getProperty("file.encoding"));
//		System.out.println("Default Charset=" + Charset.defaultCharset());

    }
}
