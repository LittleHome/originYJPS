package com.yjps.security;

import com.yjps.exception.DocumentException;
import com.yjps.model.Role;
import com.yjps.model.User;
import com.yjps.service.IUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;

/**
 * http://www.cnblogs.com/sharpxiajun/p/3151510.html 前端
 * Created by pcc on 2015/7/12.
 */

@Service
@Transactional
public class MyRealm extends AuthorizingRealm implements Serializable{

    /*
    如果写成：
    @Resource
    private UserService userService;
    会报异常：Caused by: org.springframework.beans.factory.BeanNotOfRequiredTypeException: Bean named 'userService' must be of type [UserService], but was actually of type [com.sun.proxy.$Proxy19]
    * */
    @Resource
    private IUserService userService;
    /**
     * 获取授权信息
     */

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("获取授权信息>>>>>>>>>>>>>>");
        //获取登录时输入的用户名
        String loginName=(String) principalCollection.fromRealm(getName()).iterator().next();
        //到数据库获取此用户
        User user=userService.findUserByName(loginName);
        if(user!=null){
            //权限信息对象info,用来存放查出的用户的所有的角色（role）及权限（permission）
            SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
            //用户的角色集合
            System.out.println("role = " + user.getRolesName());
            info.setRoles(user.getRolesName());
            //用户的角色对应的所有权限，如果只使用角色定义访问权限
            Collection<Role> roleList=user.getRoles();
            for (Role role : roleList) {
                info.addStringPermissions(role.getPermissionsName());
            }

            return info;
        }
        return null;
    }

/**
     * 获取身份验证相关信息
     */

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authenticationToken) throws AuthenticationException {

        System.out.println(">>>>>myrealm>>>>>>");
        String username = authenticationToken.getPrincipal().toString();
        User user = userService.findUserByName(username);
        if (null == user)
        {
            throw new DocumentException("没有相关用户!");
        }

        String principal = username;
        String hashedCredentials = user.getPassword();
        ByteSource credentialsSalt = ByteSource.Util.bytes(user.getUsername() + PasswordHelper.ps);

        String realmName = getName();
        SimpleAuthenticationInfo authentication = new SimpleAuthenticationInfo(principal, hashedCredentials,credentialsSalt,realmName);
        return authentication;
    }
    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }
}
