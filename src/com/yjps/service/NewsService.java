package com.yjps.service;

import com.yjps.exception.DocumentException;
import com.yjps.model.News;
import com.yjps.model.Pager;
import com.yjps.model.User;
import com.yjps.dao.INewsDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pcc on 2015/7/15.
 */
@Service("newsService")
public class NewsService implements INewsService{
    @Resource
    private INewsDao newsDao;

    @Override
    public Pager<News> listAllNews() {
        String hql = "from News";
        return newsDao.find(hql);
    }

    @Override
    public void addNews(News news,User user) {
//        HttpServletRequest request = LoginUserUtil.getMember();
        if(user == null){
            throw new DocumentException("请重新登陆");
        }
        if(news != null){
//            User user = (User) request.getSession().getAttribute("loginUser");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            news.setCreate_time(format.format(new Date()));
            news.setState(1);
            news.setSenderId(user.getId());
            newsDao.add(news);
        }else{
            throw new DocumentException("新闻为空值");
        }
    }

    @Override
    public void deleteNews(int id) {
        newsDao.delete(id);
    }

    @Override
    public News updateNews(News news) {
        newsDao.update(news);
        return newsDao.load(news.getId());
    }


    @Override
    public News findNews(int id) {
        News news = newsDao.load(id);
        return news;
    }


}
