package com.yjps.service;

import com.yjps.dao.IRoleDao;
import com.yjps.model.Pager;
import com.yjps.exception.DocumentException;
import com.yjps.model.Role;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by jessy on 2015/7/16.
 */
@Service("roleService")
public class RoleService implements IRoleService{
    private Logger log = LoggerFactory.getLogger(RoleService.class);
    @Resource
    private IRoleDao roleDao;

    @Override
    public void addRole(Role role) {
        roleDao.add(role);
        log.info("添加角色 " + role.getRolename() + " 成功！");
    }

    @Override
    public void deleteRole(int id) {
        roleDao.delete(id);
        log.info("删除角色id为 " + id + "成功！");
    }

    @Override
    public Role updateRole(Role role) {
        try {
            roleDao.update(role);
            BeanUtils.copyProperties(role, roleDao.load(role.getId()));

        }catch (NullPointerException e){
            throw new DocumentException("更新角色对象为空！");
        } catch (InvocationTargetException e) {
            throw new DocumentException("系统异常>>>");
        } catch (IllegalAccessException e) {
            throw new DocumentException("系统异常>>>");
        }
        return role;
    }

    @Override
    public List<Role> roleList() {
        String hql = "from Role r";
        return roleDao.list(hql);
    }

    @Override
    public Pager<Role> rolePagerList() {
        String hql = "from Role r";
        return roleDao.find(hql);
    }

    @Override
    public Role findRole(int id) {
        return roleDao.load(id);
    }


}
