package com.yjps.service;



import com.yjps.model.Pager;
import com.yjps.model.User;

import java.util.List;

public interface IUserService {
	public void addUser(User user);
	public void deleteUser(int id);
	public User updateUser(User user);
	public Pager<User> findUserPager();
	public List<User> findUserList();
	public User findUserByName(String username);
	public User findUser(int id);

}
