package com.yjps.service;

import com.yjps.dao.IUserDao;
import com.yjps.model.Pager;
import com.yjps.model.User;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserService implements IUserService {
	@Resource
	private IUserDao userDao;


	@Override
	public void addUser(User user) {
		userDao.add(user);
	}

	@Override
	public void deleteUser(int id) {
       userDao.delete(id);
	}

	@Override
	public User updateUser(User user) {
		userDao.update(user);
		return userDao.load(user.getId());
	}

	@Override
	public Pager<User> findUserPager() {
		String hql = "from User u";
		return userDao.find(hql);
	}

	@Override
	public List<User> findUserList() {
		String hql = "from User u";
		return userDao.list(hql);
	}

	@Override
	public User findUserByName(String username) {
		String hql = "from User u where u.username=?";
		return (User)userDao.queryByHql(hql,username);
	}

	@Override
	public User findUser(int id) {
		return  userDao.load(id);
	}

}
