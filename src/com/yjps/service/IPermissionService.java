package com.yjps.service;

import com.yjps.model.Pager;
import com.yjps.model.Permission;

import java.util.List;

/**
 * Created by jessy on 2015/7/16.
 */
public interface IPermissionService {
    public Permission findPermission(int id);
    public List<Permission> permissionList();
    public Pager<Permission> permissionPagerList();
    public void addPermission(Permission permission);
    public Permission updatePermission(Permission permission);
    public void deletePermisson(int id);
}
