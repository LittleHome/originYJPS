package com.yjps.service;

import com.yjps.model.Pager;
import com.yjps.model.Role;

import java.util.List;

/**
 * Created by pcc on 2015/7/16.
 */
public interface IRoleService {
    public void addRole(Role role);
    public void deleteRole(int id);
    public Role updateRole(Role role);
    public List<Role> roleList();
    public Pager<Role> rolePagerList();
    public Role findRole(int id);
}
