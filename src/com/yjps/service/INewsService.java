package com.yjps.service;

import com.yjps.model.News;
import com.yjps.model.Pager;
import com.yjps.model.User;


/**
 * Created by pcc on 2015/7/15.
 */
public interface INewsService {
    public Pager<News> listAllNews();
    public void addNews(News news,User user);
    public void deleteNews(int id);
    public News updateNews(News news);
    public News findNews(int id);
}
