package com.yjps.service;

import com.yjps.model.Pager;
import com.yjps.model.Permission;
import com.yjps.dao.IPermissionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by jessy on 2015/7/16.
 */
@Service("permissionService")
public class PermissionService implements IPermissionService {
    private Logger log = LoggerFactory.getLogger(PermissionService.class);
    @Resource
    private IPermissionDao permissionDao;

    @Override
    public Permission findPermission(int id) {
        return permissionDao.load(id);
    }

    @Override
    public List<Permission> permissionList() {
        String hql = "from Permission p";
        return permissionDao.list(hql);
    }

    @Override
    public Pager<Permission> permissionPagerList() {
        String hql = "from Permission p";
        return permissionDao.find(hql);
    }

    @Override
    public void addPermission(Permission permission) {
        permissionDao.add(permission);
    }

    @Override
    public Permission updatePermission(Permission permission) {
        permissionDao.update(permission);
        return permissionDao.load(permission.getId());
    }

    @Override
    public void deletePermisson(int id) {
        permissionDao.delete(id);
    }
}
