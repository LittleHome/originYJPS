package com.yjps.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private int id;
	private String username;
	private String password;
	private String mobile;
	private String email;
	@Column(name = "realname")
	private String realName;
	private int state;
	@OneToMany(targetEntity = Role.class,fetch = FetchType.LAZY)
	@JoinTable(name = "user_role",joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

	public User() {
	}
	
	public User(int id) {
		super();
		this.id = id;
	}

	public User(int id, String username) {
		super();
		this.id = id;
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Transient
	public Set<String> getRolesName(){
		HashSet<String> rolesNameSet = new HashSet<>();
		for(Role role:roles)
			rolesNameSet.add(role.getRolename());
		return rolesNameSet;
	}
}
