package com.yjps.model;

import javax.persistence.*;

/**
 * Created by pcc on 2015/7/15.
 */
@Entity
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue
    @Column(name = "news_id")
    private int id;
    /**
     * 新闻接收者id
     */
    @Column(name = "sender_id")
    private int senderId;


    private String content;
    private String create_time;
    private String title;
    private int state;

    public News() {
    }

    public News(int id) {
        super();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }
}
