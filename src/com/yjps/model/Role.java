package com.yjps.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pcc on 2015/7/12.
 */
@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue
    @Column(name = "role_id")
    private int id;
    private String rolename;
    @Column(name = "description")
    private String role_description;
    private int state;
    @OneToMany(targetEntity = Permission.class,fetch=FetchType.LAZY)
    @JoinTable(name = "role_permission", joinColumns = {@JoinColumn(name = "role_id")}, inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    private List<Permission> permissions;
    @Transient
    private String viewPermissions;

    @Transient
    private List<Permission> allPermissions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRole_description() {
        return role_description;
    }

    public void setRole_description(String role_description) {
        this.role_description = role_description;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }
    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getViewPermissions() {
        return viewPermissions;
    }

    public void setViewPermissions(String viewPermissions) {
        this.viewPermissions = viewPermissions;
    }

    public List<Permission> getAllPermissions() {
        return allPermissions;
    }

    public void setAllPermissions(List<Permission> allPermissions) {
        this.allPermissions = allPermissions;
    }

    @Transient
    public Set<String> getPermissionsName(){
        HashSet<String> permissionsNameSet = new HashSet<>();
//        for(Permission permission:permissions){
//            permissionsNameSet.add(permission.getPermission());
//        }
        return permissionsNameSet;
    }
}
