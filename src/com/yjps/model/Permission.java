package com.yjps.model;

import javax.persistence.*;

/**
 * Created by pcc on 2015/7/12.
 */
@Entity
@Table(name = "permission")
public class Permission {
    @Id
    @GeneratedValue
    @Column(name = "permission_id")
    private int id;
    @Column(name = "permission")
    private String permissionName;
    @Column(name = "description")
    private String per_description;
    private int state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getPer_description() {
        return per_description;
    }

    public void setPer_description(String per_description) {
        this.per_description = per_description;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
