package com.yjps.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.yjps.exception.DocumentException;
import com.yjps.model.User;
import com.yjps.service.IUserService;
import com.yjps.util.ActionUtil;
import com.yjps.util.LoginUserUtil;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;

/**
 * 登录业务控制类
 */
@Controller("loginAction")
@Scope("prototype")
public class LoginAction extends ActionSupport{
	Logger log = Logger.getLogger(LoginAction.class);

	private String username;
	private String password;
	private String url;
	@Resource
	private IUserService userService;


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String loginInput() {
		log.debug("request login page");
		return "login";
	}

	/**
	 * 登录
	 * @return
	 */
	public String login() {
		System.out.println("username = " + username + "  password = " + password);
		if((!StringUtils.isEmpty(username)) && (!StringUtils.isEmpty(password))) {
			try {
				UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username.trim(), password.trim());
				SecurityUtils.getSubject().login(usernamePasswordToken);
				User u = userService.findUserByName(username);

				/*当前登录用户保存在Cookie*//*
				Cookie userCookie = new Cookie("loginUser",u.getUsername()+u.getPassword());
				userCookie.setPath("/");
				userCookie.setMaxAge(3600824 * 30);*/

//				ActionContext.getContext().getSession().put("loginUser", u);
				ServletActionContext.getRequest().getSession().setAttribute("loginUser", u);
				LoginUserUtil.setMember(ServletActionContext.getRequest());
				if(LoginUserUtil.getMember() != null)
				System.out.println("LoginUserUtil.getMember = " + LoginUserUtil.getMember().getSession().getAttribute("loginUser"));

				//选择shiro的successURL跳转
				ActionUtil.setUrl("/login!home.action");
				LoginUserUtil.setMember(ServletActionContext.getRequest());
				return ActionUtil.REDIRECT;
			} catch (UnknownAccountException uae) {
				throw new DocumentException("Unknown User!");
			} catch (IncorrectCredentialsException ice) {
				throw new DocumentException("Incorrect Password!");
			} catch (LockedAccountException lae) {
				throw new DocumentException("User Locked!");
			} catch (AuthenticationException ae) {
				throw new DocumentException("Authentication Failed!");
			}
		}else{
			throw new DocumentException("用户名或密码不能为空 !");
		}
	}

	/**
	 * 跳转到主页
	 * @return
	 */
	public String home(){
		return "home";
	}

	/**
	 * 退出
	 * @return
	 */
	public String logout() {
		SecurityUtils.getSubject().logout();
		ActionContext.getContext().getSession().clear();
		LoginUserUtil.removeMember();
		ActionUtil.setUrl("/login!loginInput.action");
		return ActionUtil.REDIRECT;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}


}
