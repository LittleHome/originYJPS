package com.yjps.action;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yjps.model.News;
import com.yjps.model.Pager;
import com.yjps.model.User;
import com.yjps.service.INewsService;
import com.yjps.util.ActionUtil;
import com.yjps.exception.DocumentException;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by jessy on 2015/7/15.
 */
@Controller("newsAction")
public class NewsAction extends ActionSupport implements ModelDriven<News>{
    private Logger log = LoggerFactory.getLogger(NewsAction.class);
    private News news;
    @Resource
    private INewsService newsService;

    @Override
    public News getModel() {
        if(news==null) news = new News();
        return news;
    }

    public User getCurrentUser(){
        User user = null;
        if(ActionContext.getContext().getSession().get("loginUser") != null){
            user = (User) ActionContext.getContext().getSession().get("loginUser");
        }
         return user;
    }

    public String newsList(){
        Pager<News> newsPager = newsService.listAllNews();
        try {
            log.info("新闻数据 ： " + newsPager.getTotalRecord());
            List<News> newsList = newsPager.getDatas();
            HttpServletRequest request = ServletActionContext.getRequest();
            request.setAttribute("newsPager", newsPager);
            return ActionUtil.LIST;
        }catch (NullPointerException e){
            throw new DocumentException("还没有新闻数据！");
        }
    }

    public String addNews(){
        log.info("添加新闻>>>>" + news);
        newsService.addNews(news,getCurrentUser());
        ActionUtil.setUrl("/news_newsList.action");
        return ActionUtil.REDIRECT;
    }

    public String deleteNews(){
        log.info("删除 id 为 " + news.getId() + "的新闻");
        newsService.deleteNews(news.getId());
        ActionUtil.setUrl("/news_newsList.action");
        return ActionUtil.REDIRECT;
    }

    public String updateNews(){
        log.info("更新 id 为 " + news.getId() + "的新闻");
        newsService.updateNews(news);
        ActionUtil.setUrl("/news_newsList.action");
        return ActionUtil.REDIRECT;
    }

    public String loadNews() throws IOException {
        System.out.println("load >>>>>>> ");
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("application/json;charset=utf-8");
        String jsonString = JSON.toJSONString(newsService.findNews(news.getId()));
        System.out.println("jsonString = " + jsonString);
        response.getWriter().write(jsonString);
//        ActionUtil.setUrl("/news_newsList.action");
        return null;
    }
}
