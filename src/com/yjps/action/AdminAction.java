package com.yjps.action;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yjps.model.Permission;
import com.yjps.exception.DocumentException;
import com.yjps.model.Pager;
import com.yjps.model.Role;
import com.yjps.model.User;
import com.yjps.security.PasswordHelper;
import com.yjps.service.IPermissionService;
import com.yjps.service.IRoleService;
import com.yjps.service.IUserService;
import com.yjps.util.ActionUtil;
import com.yjps.util.LoginUserUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统管理业务逻辑控制类
 * Created by jessy on 2015/7/16.
 */
@Controller("adminAction")
public class AdminAction extends ActionSupport implements ModelDriven<User>{
    private Logger log = LoggerFactory.getLogger(AdminAction.class);
    @Resource
    private IUserService userService;
    @Resource
    private IRoleService roleService;
    @Resource
    private IPermissionService permissionService;


    private User user;
    private Role role;
    private Permission permission;

    private String[] permissions;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Override
    public User getModel() {
        if(user == null){
            return new User();
        }else{
            return user;
        }
    }

    private User getCurrentUser(){
            User user = (User) LoginUserUtil.getMember().getSession().getAttribute("loginUser");
            return user;
    }
    /**
     * 添加角色
     * @return  跳转至角色列表
     */
    public String addRole(){
        log.info("添加角色 ");
        System.out.println("permissions = " + permissions);
        HttpServletRequest request = ServletActionContext.getRequest();
        String[] permissions = request.getParameterValues("permissions");
        List<Permission> permissionsList = new ArrayList<>();
        if(permissions != null)
            for(String permission : permissions){
                if(!StringUtils.isEmpty(permission)){
                   Permission permission_temp = permissionService.findPermission(Integer.parseInt(permission));
                   permissionsList.add(permission_temp);
            }
        }
        /*Role role = new Role();
        role.setRole_description(role_description);
        role.setRolename(rolename);
        */
        role.setState(1);
        role.setPermissions(permissionsList);
        roleService.addRole(role);
        ActionUtil.setUrl("/admin_roleList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * 显示所有角色
     * @return
     */
    public String roleList(){
        try {
            log.info("角色列表");
            List<Role> roleList = roleService.roleList();
            for(Role r : roleList){
                System.out.println("r =  " + r.getRolename());
                StringBuilder s = new StringBuilder();
                for(Permission p : r.getPermissions()){
                    s.append(p.getPer_description()).append("; ");
                }
                r.setViewPermissions(s.toString());
            }
            for(Role r : roleList) {
                System.out.println("p =  " + r.getViewPermissions());
            }
            HttpServletRequest request = ServletActionContext.getRequest();
            request.setAttribute("roleList", roleList);
            return ActionUtil.LIST;
        }catch (NullPointerException e){
            throw new DocumentException("没有数据");
        }
    }

    /**
     * json数据格式请求所有权限列表
     * @return  权限列表json数据
     */
    public String findAllPermissions(){
        try {
            List<Permission> permissionList = permissionService.permissionList();
            String jsonString = JSON.toJSONString(permissionList);
            System.out.println("json = " + jsonString);
            HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().write(jsonString);
        } catch (IOException e) {
            throw new DocumentException("系统异常");
        }
        return null;
    }

    /**
     * 删除指定id的角色
     * @return 跳转回角色列表
     */
    public String deleteRole(){
        log.info("删除角色 id 为 " + role.getId());
        roleService.deleteRole(role.getId());
        ActionUtil.setUrl("/admin_roleList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * 更新指定角色
     * @return 跳转回角色列表
     */
    public String updateRole(){
        roleService.updateRole(role);
        ActionUtil.setUrl("/admin_roleList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * json数据格式请求指定id的角色对象
     * @return
     * @throws IOException
     */
    public String loadRole() throws IOException {
        System.out.println("load >>>>>>> ");
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("application/json;charset=utf-8");
        Role r = roleService.findRole(role.getId());
            StringBuilder s = new StringBuilder();
            for(Permission p : r.getPermissions()){
                s.append(p.getPer_description()).append("; ");
            }
            r.setViewPermissions(s.toString());

        List<Permission> permissionList = permissionService.permissionList();
        r.setAllPermissions(permissionList);

        String jsonString = JSON.toJSONString(r);

        System.out.println("jsonString = " + jsonString);
        response.getWriter().write(jsonString);
//        ActionUtil.setUrl("/news_newsList.action");
        return null;
    }

    /**
     * 显示所有的权限
     * @return
     */
    public String permissionList(){
        List<Permission> permissionList = permissionService.permissionList();
        HttpServletRequest request = ServletActionContext.getRequest();
        request.setAttribute("permissionList", permissionList);
        return ActionUtil.LIST;
    }

    /**
     * 添加权限
     * @return  跳转回权限列表
     */
    public String addPermission(){
        permission.setState(1);
        permissionService.addPermission(permission);
        ActionUtil.setUrl("/admin_permissionList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * 更新指定的权限对象
     * @return 跳转回权限列表
     */
    public String updatePermission(){
        permissionService.updatePermission(permission);
        ActionUtil.setUrl("/admin_permissionList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * json数据格式请求指定id的权限对象
     * @return
     * @throws IOException
     */
    public String loadPermission() throws IOException {
        System.out.println("load >>>>>>> ");
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("application/json;charset=utf-8");
        Permission p = permissionService.findPermission(permission.getId());
        String jsonString = JSON.toJSONString(p);

        System.out.println("jsonString = " + jsonString);
        response.getWriter().write(jsonString);
//        ActionUtil.setUrl("/news_newsList.action");
        return null;
    }

    /**
     * 删除指定id的权限对象
     * @return 跳转回权限列表
     */
    public String deletePermission(){
        permissionService.deletePermisson(permission.getId());
        ActionUtil.setUrl("/admin_permissionList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * 分页显示用户列表
     * @return
     */
    public String userList(){
        if(LoginUserUtil.getMember() != null)
            System.out.println("adminAction > LoginUserUtil.getMember = " + LoginUserUtil.getMember().getSession().getAttribute("loginUser"));

        Pager<User> userPager = userService.findUserPager();
        HttpServletRequest request = ServletActionContext.getRequest();
        request.setAttribute("userPager", userPager);
        return ActionUtil.LIST;
    }

    /**
     * 添加用户
     * @return
     */
    public String addUser(){
        try {
            System.out.println("user .." + user.getUsername());
            user.setState(1);
            user.setPassword("111");
            user = PasswordHelper.generatePassword(user);
            userService.addUser(user);
            ActionUtil.setUrl("/admin_userList.action");
            return ActionUtil.REDIRECT;
        }catch (NullPointerException e){
            throw new DocumentException("添加失败！");
        }
    }

    /**
     * 删除用户
     * @return
     */
    public String deleteUser(){
        userService.deleteUser(user.getId());
        ActionUtil.setUrl("/admin_userList.action");
        return ActionUtil.REDIRECT;
    }

    /**
     * 更新用户
     * @return
     */
    public String updateUser(){
        User u = userService.updateUser(user);
        ActionUtil.setUrl("/admin_userList.action");
        return ActionUtil.REDIRECT;

    }

    /**
     * json数据格式 请求用户对象
     * @return
     * @throws IOException
     */
    public String loadUser() throws IOException {
        System.out.println("load >>>>>>> ");
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("application/json;charset=utf-8");
        User u = userService.findUser(user.getId());

        String jsonString = JSON.toJSONString(u);

        System.out.println("jsonString = " + jsonString);
        response.getWriter().write(jsonString);
//        ActionUtil.setUrl("/news_newsList.action");
        return null;
    }
}
