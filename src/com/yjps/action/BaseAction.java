package com.yjps.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;

/**
 * Created by jessy on 2015/7/13.
 */
public abstract class BaseAction<T> extends ActionSupport implements ServletRequestAware,ServletResponseAware{
    private final Logger log = LoggerFactory.getLogger(BaseAction.class);

    protected Subject currentUser;
    protected Class modelClass;

//    protected void setPages(Page<T> page)

    public BaseAction(){
        ParameterizedType pt = (ParameterizedType)this.getClass().getGenericSuperclass();
        modelClass = (Class)pt.getActualTypeArguments()[0];
        currentUser = SecurityUtils.getSubject();
    }

    /*public String toAdd() throws Exception {
        if (currentUser.isPermitted(getPremissionModelCode())) {
            return "";

        }
    }*/
}
