package com.yjps.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yjps.model.User;
import com.yjps.service.IUserService;
import com.yjps.util.ActionUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by jessy on 2015/7/13.
 */
@Controller("userAction")
public class UserAction extends ActionSupport implements ModelDriven<User> {
    private User user;
    private String url;
    private String id;
    @Resource
    private IUserService userService;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /**
     * 显示个人信息
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public String showSelf() throws IllegalAccessException, InvocationTargetException {
        User tu = (User) ActionContext.getContext().getSession().get("loginUser");
        BeanUtils.copyProperties(user, tu);
        return SUCCESS;
    }

    /**
     * 编辑个人信息
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public String edit() throws InvocationTargetException, IllegalAccessException {
        System.out.println("mobile = " + user.getMobile());
        userService.updateUser(user);
        User u = userService.findUser(user.getId());
        BeanUtils.copyProperties(user,u);
        ActionUtil.setUrl("/user_showSelf.action");
        return ActionUtil.REDIRECT;
    }


    @Override
    public User getModel() {
        if(user==null) user = new User();
        return user;
    }
}
