package com.yjps.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jessy on 2015/7/15.
 */
public class LoginUserUtil {
    private static ThreadLocal<HttpServletRequest> currentLoginUser = new ThreadLocal<>();

    public LoginUserUtil() {
    }

    public static HttpServletRequest getMember() {
        if(currentLoginUser.get() != null)
            return currentLoginUser.get();
        else return null;
    }

    public static void setMember(HttpServletRequest m){
        currentLoginUser.set(m);
    }

    public static void removeMember(){

        currentLoginUser.remove();
    }
}
