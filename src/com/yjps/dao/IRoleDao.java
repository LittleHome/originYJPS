package com.yjps.dao;

import com.yjps.model.Role;

/**
 * Created by jessy on 2015/7/16.
 */
public interface IRoleDao extends IBaseDao<Role>{
}
