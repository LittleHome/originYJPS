package com.yjps.dao;

import com.yjps.model.News;
import org.springframework.stereotype.Repository;

/**
 * Created by pcc on 2015/7/15.
 */
@Repository("newsDao")
public class NewsDao extends BaseDao<News> implements INewsDao{
}
