package com.yjps.dao;

import com.yjps.model.News;

/**
 * Created by pcc on 2015/7/15.
 */
public interface INewsDao extends IBaseDao<News>{
}
