package com.yjps.dao;


import com.yjps.model.Permission;

/**
 * Created by pcc on 2015/7/16.
 */
public interface IPermissionDao extends IBaseDao<Permission>{
}
