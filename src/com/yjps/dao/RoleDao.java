package com.yjps.dao;

import com.yjps.model.Role;
import org.springframework.stereotype.Repository;

/**
 * Created by jessy on 2015/7/16.
 */
@Repository("roleDao")
public class RoleDao extends BaseDao<Role> implements IRoleDao{
}
