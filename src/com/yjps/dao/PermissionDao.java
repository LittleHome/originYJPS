package com.yjps.dao;

import com.yjps.model.Permission;
import org.springframework.stereotype.Repository;

/**
 * Created by jessy on 2015/7/16.
 */
@Repository("permissionDao")
public class PermissionDao extends BaseDao<Permission> implements IPermissionDao{
}
