package com.yjps.web;

import com.yjps.model.User;
import com.yjps.util.LoginUserUtil;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by pcc on 2015/7/24.
 */
public class SecurityManageFilter extends AuthorizationFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        System.out.println("filter---------------------------------------------");
        User loginUser = (User)httpRequest.getSession().getAttribute("loginUser");
        //拦截
        if (loginUser != null) {
         LoginUserUtil.setMember(httpRequest);
            return true;
        }else{
            //该用户还没登录
            System.out.println("have not login....");
            saveRequestAndRedirectToLogin(request, response);
        }
        return false;
    }
}
