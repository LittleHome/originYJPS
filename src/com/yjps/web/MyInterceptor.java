package com.yjps.web;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * Created by pcc on 2015/7/25.
 */
@Aspect
public class MyInterceptor {


    //声明一个切入点
//    @Pointcut(value = "execution (* com.yjps.service.NewsService.*(..))")
    private void anyMethod(){}

//    @Before("anyMethod() && args(name)")
//    @Before("anyMethod()")
    public void doAccessCheck(/*String name*/){
        System.out.println("前置通知....");
    }
//    @AfterReturning(pointcut = "anyMethod()",returning = "result")
//    @AfterReturning(pointcut = "anyMethod()")
    public void doAfterReturning(/*String result*/){
        System.out.println("后置通知...");
    }
//    @After("anyMethod()")
    public void doAfter(){
        System.out.println("最终通知...");
    }
//    @AfterThrowing(pointcut = "anyMethod()",throwing = "e")
//    @AfterThrowing(pointcut = "anyMethod()")
    public void doAfterThrowing(/*Exception e*/){
        System.out.println("异常通知...");
    }
//    @Around("anyMethod()")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable{
//        if()可以判定用户是否有权限进行权限控制
        System.out.println("enter...");
        Object result = pjp.proceed();
        System.out.println("leave...");
        return result;
    }
}
